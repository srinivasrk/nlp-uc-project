ab: about
abt: about.
adventuritter:a Twitterer who is adventurous.
attwaction: attraction between two users.
attwicted:someone who is addicted to Twitter.
Awsum:awesome
b/c: because
bcoz:because
B: be
b4: before
Bberrytweet: Blackberry device to send a tweet on Twitter.
beetweet: hot tweet
BFN: bye for now
bgd:background.
Bird-of-mouth: use of Twitter to circulate news and information.
BR: best regards
BTW:by the way
bulltwit: false
Celebrity syndrome: a non-celebrity mistakenly believes he or she is a celebrity. 
chk: check
cld: could
clk: click
crank tweet: misleading tweet.
cre8: create
da: the
detweet: Slang term used to describe a tweet you made, then deleted.
DM: direct message
drunktwittering: The act of posting on Twitter while intoxicated.
Dweet: tweet that has been sent by a user who is drunk.
eavestweeting: The act of eavesdropping on other Twitter conversations.
egotwistical: a user who talks about himself on Twitter. It is a combination of the words "egotistical and Twitter."
EM: e-mail
eml: e-mail
EMA: e-mail addresser.
emergaTweet: Also known as emergetweet it refers to a tweet sent out during an emergency when 911 is unavailable.
F2F: face to face
fab: fabulous
Fail Whale: On the Twitter site, the Fail Whale is an image of a whale held up by birds and nets. This image shows that Twitter has been overloaded or that a failure occurred within the service.
FAV: favoritee.

FOMO: fear of missing out

FTL:for the loss
FTW: for the win
fyi: for your information
fml:fuck my life

HAND:have a nice day
H.O: Hang over
IC: I see
ICYMI: in case you missed 
idk: I don’t know
ITZ:It is
intwituation/intwituated:  infatuated with another Twitter user.
kk: cool
NTS: note to self
OH: overhead
PRT: please retweet
SMH: shaking my head/ which is typically used when someone does not agree with something or is disgusted or off-put by something.
SP: Sponsored
TBH: to be honest
TBT: Throw Back Thursday
TMB: Tweet me back
twabe : dear
twabulous: = fabulous tweet.
Twaffic: Twitter traffic
twis: this
twittcrastination:  procrastination
Twitter-ific: terrific
U: you
woz: was
wtv:whatever
YOLO: enjoy life.
YOYO: you’re on your own
ztwitt: to tweet extremely fast
wcw: women crush wednesday
ff: follow friday
DAM = Don't annoy me
DD = Dear daughter
DF = Dear fiancé
DAM: dont anger me
omg: oh my god
lol: laugh out loud
rofl: rolling on the floor laughing
zomg:oh my god to the max
